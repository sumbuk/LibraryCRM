module.exports = {
  JWT_SECRET: "jwt-secret",
  PORT: 4000,
  MONGODB_URI: "mongodb://localhost:27017/restify-api",
  ROLE_ADMIN: 2,
  ORDER_STATUS_SUCCESS: 1,
  ORDER_ALREADY_COMPLETED: 2,
  ORDER_STATUS_NEW: 0,
  ORDER_STATUS_REJECTED: 3
};
